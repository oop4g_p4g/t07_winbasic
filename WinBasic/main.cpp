#include <windows.h>
#include <string>
#include <cassert>
#include <iostream>

#include "WindowUtils.h"

using namespace std;

WinUtil wu;



//anything that needs to happen just once at the start
void InitGame()
{
	
}

//any memory or resources we made need releasing at the end
void ReleaseGame()
{
	
}

//called over and over, use it to update game logic
void Update(float dTime)
{
	
}

//called over and over, use it to render things
void Render(float dTime)
{
	
}


//messages come from windows all the time, should we respond to any specific ones?
LRESULT CALLBACK MainWndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{

	//default message handling (resize window, full screen, etc)
	return wu.DefaultMssgHandler(hwnd, msg, wParam, lParam);
}

//main entry point for the game
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE prevInstance, PSTR cmdLine, int showCmd)
{
	if (!wu.InitMainWindow(1024, 768, hInstance, "Fezzy", MainWndProc))
		assert(false);

	InitGame();
	
	wu.Run(Update, Render);

	ReleaseGame();
	
	return 0;
}