#ifndef WINDOWSUTILS
#define WINDOWSUTILS

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <string>
#include <sstream>
#include <assert.h>




class MyD3D;

/*
Manage the Microsoft Windows side of the game app
*/
class WinUtil
{
private:
	struct WinData
	{
		//all windows apps have these handles
		HINSTANCE hAppInst = 0;
		HWND      hMainWnd = 0;
		std::string mainWndCaption;
		int clientWidth;
		int clientHeight;
	};
	WinData mWinData;

public:
	//setup a window to render into
	bool InitMainWindow(int width, int height, HINSTANCE hInstance, const std::string& appName, WNDPROC mssgHandler);
	//handle messages from the operating system
	LRESULT DefaultMssgHandler(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
	const WinData& GetData() const {
		return mWinData;
	}

	/*
	wrap your game in one function call if using basic functions
	pUpdate = a function pointer to a free function that updates your logic
	pRender = a fucntion pointer to a free function that renders your simulation
	return the last windows message parameter we handled, sometime useful but safe to ignore
	*/
	int Run(void(*pUpdate)(float), void(*pRender)(float));

	//alternatively you can run your game this way
	/*
	Call this before your Update/Render, and only call Update/Render
	if the bool canUpdateRender is true.
	Returns false if windows is asking us to quit.
	*/
	bool BeginLoop(bool& canUpdateRender);
	/*
	Call this after your Update/Render.
	Save that canUpdateRender variable you used earlier and pass it through.
	Return the elapsed time for this update in seconds (so it will usually be a very small number)
	*/
	float EndLoop(bool didUpdateRender);

};

#endif
